"use strict";

const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]
const putOnPage = (array, listParent = document.body) => {
    const list = document.createElement("ul")
    array.map(content => {
        const li = document.createElement('li');
        li.textContent = content;
        list.appendChild(li)
    })
    listParent.appendChild(list)
}
const createPathArg =(listParent)=>{
    return document.querySelector(`.${listParent}`)
}
putOnPage(array, createPathArg("it-is-a-list"))
